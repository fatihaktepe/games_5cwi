package Remote;

public class Remote {
	private boolean isOn;
	private boolean hasPower;
	private Battery battery1;
	private Battery battery2;

	public Remote(boolean isOn, boolean hasPower, Battery hasBattery, Battery hasBattery1) {
		super();
		this.isOn = isOn;
		this.setHasPower(hasPower);
		this.battery1 = hasBattery;
		this.battery2 = hasBattery1;
	}

	public boolean isRemoteOn() {
		return isOn;
	}

	public void turnOn() {
		isOn = true;
	}

	public void turnOff() {
		isOn = false;
	}

	public double getStatus() {
		return (battery1.getChargingStatus() + battery2.getChargingStatus()) / 2.0;
	}

	public boolean isHasPower() {
		return hasPower;
	}

	public void setHasPower(boolean hasPower) {
		this.hasPower = hasPower;
	}
}
