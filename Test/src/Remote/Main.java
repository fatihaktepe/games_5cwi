package Remote;

public class Main {
	public static void main(String[] args) {
		Battery B1 = new Battery(50);
		Battery B2 = new Battery(30); 
		Battery B3 = new Battery(0);
		Battery B4 = new Battery(5);

		Remote R1 = new Remote(false, false, B1,B2); 
		Remote R2 = new Remote(true, true, B3, B4); 
		Remote R3 = new Remote(true,true,B1,B3);
		
		System.out.println(R1.getStatus());
		System.out.println(R2.getStatus());
		System.out.println(R3.getStatus());
	}

}
