package CarSample;

public class Car {

	private String color;
	private int maxPs; 
	private int price;
	private int basicUse;
	private Producer producer; 
	private Engine engine;
	private Person owner;
	
	
	public Car(String color, int maxPs, int price, int basisverbrauch, Producer producer, Engine engine, Person owner) {
		super();
		this.color = color;
		this.maxPs = maxPs;
		this.price = price;
		this.basicUse = basisverbrauch;
		this.producer = producer;
		this.engine = engine;
		this.owner = owner;
 
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getMaxPss() {
		return maxPs;
	}

	public void setMaxPs(int maxPs) {
		this.maxPs = maxPs;
	}

	public int getPrice() {
		return price;
	}
	
	public int getPriceWithDiscount() {
		// TODO use correct discount from producer
		return (int) ((1.0 - this.producer.getRabatte()) * this.price);
	} 
	
	public void setPrice(int price) {
		this.price = price;
	}

	public int getBasisverbrauch() {
		return basicUse;
	}

	public void setBasisverbrauch(int basisverbrauch) {
		this.basicUse = basisverbrauch;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
	public Person getPerson() {
		return owner;
	}

	public void setPerson(Person person) {
		this.owner = person;
	}

	public void printCar() {
		System.out.println("\n next Car:" + this.basicUse + "\n" + this.color + "\n" + this.maxPs + "\n" + this.price + "\n" + this.engine.getPower() + "\n" + this.producer.getRabatte() + "\n" + this.getPerson().getLastName());
		
	}
 
}
