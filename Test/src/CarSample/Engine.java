package CarSample;

public class Engine {
	 
	private boolean isDiesel = false; 
	private int power;
		
	public Engine(boolean isDiesel, int power) {
		super();
		this.isDiesel = isDiesel;
		this.power = power;
	}
	public boolean isDiesel() {
		return isDiesel;
	}
	public void setDiesel(boolean isDiesel) {
		this.isDiesel = isDiesel;
	}
	public int getPower() {
		return power;
	}
	public void setPower(int power) {
		this.power = power;
	}
	
	
}

