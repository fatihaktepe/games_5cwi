package CarSample;

public class Main {
	public static void main(String[] args) {
		Engine e1 = new Engine(true,200);
		Producer p1 = new Producer("VW", "Germany", 0.1);
		Engine e2 = new Engine(false, 500);
		Producer  p2 = new Producer("BMW", "Austria", 0.05); 
		
		Person per1 = new Person("Gerhard", "M�ller");
		Person per2 = new Person("Susi", "Maier");
		Person per3 = new Person("Cepo", "Hammerer");
	
		
		Car c1 = new Car("green",100,10000,6,p1,e1, per1);
		
		
		
		Car c2 = new Car("blue",500, 50000, 7, p2, e2, per2);
		Car c3 = new Car("red",700, 70000, 15, p2, e1, per3); 
		
		System.out.println(c1.getPriceWithDiscount());
		System.out.println(c2.getPriceWithDiscount());
		System.out.println(c3.getPriceWithDiscount());
		
		
		
		per1.addCar(c1);
		per1.addCar(c2);
		per1.addCar(c3);
				
		System.out.println(per1.getValueOfCars());
		

		
	}
}
