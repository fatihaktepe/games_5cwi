package CarSample;

public class Producer {
	
	private String name; 
	private String homeCountry; 
	private double rabatte;
	
	
	public Producer(String name, String homeCountry, double rabatte) {
		super();
		this.name = name;
		this.homeCountry = homeCountry;
		this.rabatte = rabatte;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHomeCountry() {
		return homeCountry;
	}
	public void setHomeCountry(String homeCountry) {
		this.homeCountry = homeCountry;
	}
	public double getRabatte() {
		return rabatte;
	}
	public void setRabatte(double rabatte) {
		this.rabatte = rabatte;
	}
	
	
}

