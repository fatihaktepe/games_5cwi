package CarSample;

import java.util.ArrayList;
import java.util.List;


public class Person {
	private String firstName, lastName;
	private List<Car> cars; 

	public Person(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cars = new ArrayList<>();
	}
	
	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getProducer().getName());
		}
	}
	
	public void addCar(Car c) {
		this.cars.add(c);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public int getValueOfCars() {
		int value = 0;
		for (Car car : cars) {
			value += car.getPriceWithDiscount();
		}
		return value; 
	}
}
